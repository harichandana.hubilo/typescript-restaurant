import React from 'react'

import {cuisine} from "./Form"

type newCuisine = {menu :cuisine[]  } & {callback : any}

const ShowMenu : React.FC<newCuisine> = React.memo(({menu,callback})  => {
    return(
        <div>
        {menu && menu.length ?
            <table>
                <thead>
                <tr>
                    <th> S.No </th>
                    <th>Cuisine Name</th>
                    <th>Cuisine Price</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    
                {menu.map((i : cuisine,index:number)=>{  
                    return(
                        <tr key={index}>
                            <td>{index+1}</td>
                            <td>{i.name}</td>
                            <td>{i.price}</td>
                            <td>
                                <button onClick={()=>callback(index)} >Delete</button>
                            </td>
                        </tr>
                    )
                })}
                </tbody>
            </table>
        
        : false}
        </div>

    )

})

export default ShowMenu