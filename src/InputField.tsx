

import React from 'react'
type inputFieldProps = {
    type : string,
    name : string,
    value : string | number,
    placeholder : string,
    callback: any,
    errortext? : string
}
 export const InputField : React.FC<inputFieldProps> = React.memo(({type,name,value,placeholder,callback, errortext}) => {
    return (
        <div>
            <input type={type} name={name} value={value} placeholder={placeholder}  onChange={callback} /><br/>
            {errortext ? <span style={{color:"red"}}> {errortext} </span>: false}
        </div>
    )
})