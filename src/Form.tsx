import React,{useState,useCallback} from 'react'
import ShowMenu from './showMenu'
import { findItemInArray } from './utils'
import {InputField} from "./InputField"

export type cuisine = {
    name : string,
    price: string
}

const Form : React.FC = React.memo(() => {
    const [menu,setMenu] = useState<cuisine[]>([])
    const [totalMenu,SetTotalMenu] = useState<cuisine[]>([])
    const [item, setItem] = useState<string>('')
    const [errortext, setErrortext] = useState<string | null>(null)
    const [filterText, setFilterText] = useState<string>('')
    const [price,setPrice] = useState<string>('')
    const [priceError,setPriceError] = useState<boolean>(false)

    const addMenu = useCallback( () : void=>  {
        if(item?.trim() === "") setErrortext('cuisine name cannot be empty')
        if(price?.trim() === "")   setPriceError(true)
        if( item?.trim() !== "" && price?.trim() !== "" ) {
            const abc : cuisine = {
                name: item,
                price
            }
            if(!findItemInArray(item,menu)) {
                setMenu((prev)=>[...prev,abc])
                SetTotalMenu((prev)=>[...prev,abc])
                setItem('')
                setPrice('')
                setErrortext(null)
            } else {
                setErrortext(`${item} already exists in cuisines list`)
            }
        }
    },[item,price,menu])    
   
    const deleteItem = useCallback( (index : number) : void => {
        let arr: cuisine[] = [...menu]
        arr.splice(index, 1);
        setMenu(arr)
        SetTotalMenu(arr)
    },[menu])

    const searchItems = useCallback((e: React.ChangeEvent<HTMLInputElement>) : void => {
        const text:string = e.target.value
        setFilterText(text)
        var filterItems : cuisine[] = []
         totalMenu.forEach((key:cuisine) => {
            if(key.name.includes(text)) filterItems.push(key)
        })
        setMenu(filterItems)
    },[totalMenu])

    const handleChange = useCallback((e: React.ChangeEvent<HTMLInputElement>) : void => {
        const name : string = e.target.name
        if(name === "item"){
            setItem(e.target.value)
            setErrortext(null)
        } 
        else{
            setPriceError(false)
            setPrice(e.target.value)
        }

    },[])

    return (
        <div>
            <h2>Add cuisines to menu</h2>
            <InputField type="text" name="item" value={item} placeholder="Enter cuisine Name" callback={handleChange} errortext = {errortext ? errortext :''} />
            <InputField type="number" name="price" value={price} placeholder="Enter cuisine Price" callback={handleChange} errortext = { priceError ?  "Cuisine price cannot be empty" : ''} />
            <button onClick={addMenu}>Add</button><br/>
            {totalMenu && totalMenu.length ? <InputField name="search" placeholder="Enter cuisine to search" type="text" value={filterText} callback={searchItems} /> : false}
            <ShowMenu 
            menu = {menu}
            callback ={deleteItem}
            />
        </div>
    )
}
)
export default Form