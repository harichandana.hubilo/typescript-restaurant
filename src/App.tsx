import React from 'react';
import Form from './Form';

const App = () => {
  return (
    <div className="App">
     <h1>Welcome to Friends Restaurants</h1>
     <Form/>
    </div>
  );
}
export default App;
