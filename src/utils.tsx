
import {cuisine} from "./Form"

export const findItemInArray = (text:string,menu : cuisine[] ) : boolean=> {
    let flag : boolean = false
     menu.forEach((key:cuisine)=>{
         if(key.name === text){
             flag = true
         } 
     })
     return flag
 } 

